/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   URI_js
 * @since     2017-10-01
 */

const Stringable = require('../Stringable')

/**
 * @class
 * @abstract
 */
class Uri extends Stringable {
}

module.exports = Uri

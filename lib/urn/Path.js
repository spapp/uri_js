/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   URI_js
 * @since     2017-09-30
 */

const Valuable = require('../Valuable')

const GLUE = ':'

class Path extends Valuable {
    static get GLUE() {
        return GLUE
    }

    /**
     * @constructor
     * @param {string} [path]
     */
    constructor(path) {
        super('string' === typeof path ? path : '')
    }
}

module.exports = Path

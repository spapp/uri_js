/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   URI_js
 * @since     2017-09-30
 */

const Valuable = require('../Valuable')

const GLUE = ':'

class Scheme extends Valuable {
    /**
     * @constant
     * @static
     * @type {string}
     */
    static get GLUE() {
        return GLUE
    }

    /**
     * @constructor
     * @param {string} [scheme]
     */
    constructor(scheme) {
        super(scheme)
    }
}

module.exports = Scheme

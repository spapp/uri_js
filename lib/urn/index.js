/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   URI_js
 * @since     2017-09-29
 */

const Uri = require('../uri')
const Scheme = require('./Scheme')
const Path = require('./Path')
const SCHEME = 'urn'

/**
 * @see {@link https://en.wikipedia.org/wiki/Uniform_Resource_Identifier}
 * @see {@link https://en.wikipedia.org/wiki/Uniform_Resource_Name}
 * @see {@link https://www.ietf.org/rfc/rfc3986.txt}
 *
 *   urn:example:mammal:monotreme:echidna
 *   └┬┘ └──────────────┬───────────────┘
 * scheme              path
 *
 * @class
 */
class Urn extends Uri {
    /**
     * @constant
     * @static
     * @type {string}
     */
    static get SCHEME() {
        return SCHEME
    }

    static get Scheme() {
        return Scheme
    }

    static get Path() {
        return Path
    }

    /**
     * @constructor
     * @param {string} [urn]
     */
    constructor(urn) {
        super()

        urn = 'string' === typeof urn ? urn : ''

        this.setScheme(new Scheme(SCHEME))
        this.setPath(new Path(urn.substr(SCHEME.length + 1)))
    }

    /**
     * Returns the path
     *
     * @return {Path}
     */
    getPath() {
        return this.path
    }

    /**
     * Sets up the path
     *
     * @param {Path} path
     * @return {Urn}
     */
    setPath(path) {
        this.path = path

        return this
    }

    /**
     * Returns the scheme
     *
     * @return {Scheme}
     */
    getScheme() {
        return this.scheme
    }

    /**
     * Sets up the scheme
     *
     * @param {Scheme} scheme
     * @return {Urn}
     */
    setScheme(scheme) {
        this.scheme = scheme

        return this
    }

    /**
     * Returns URN as a string
     *
     * @return {string}
     */
    toString() {
        const scheme = this.getScheme().toString()
        const path = this.getPath().toString()

        return path ? [ scheme, path ].join(Scheme.GLUE) : ''
    }
}

module.exports = Urn

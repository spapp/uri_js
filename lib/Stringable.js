/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2020
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   URI_js
 * @since     2020-12-16
 */

/**
 * @class
 * @interface
 */
class Stringable {

    /**
     * Returns as a string
     *
     * @return {string}
     */
    toString() {
        throw new Error('Method "toString" must be implemented')
    }
}

module.exports = Stringable

module.exports = {
    Stringable: require('./Stringable'),
    Valuable: require('./Valuable'),
    Uri: require('./uri'),
    Url: require('./url'),
    Urn: require('./urn')
}

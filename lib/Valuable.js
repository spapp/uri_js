/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   URI_js
 * @since     2017-09-30
 */

const Stringable = require('./Stringable')

/**
 * @class
 * @abstract
 */
class Valuable extends Stringable {
    constructor(value) {
        super()

        this.value = value
    }

    /**
     * Returns as a string
     *
     * @return {string}
     */
    toString() {
        return this.value + ''
    }
}

module.exports = Valuable

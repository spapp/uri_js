/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   URI_js
 * @since     2017-09-20
 */

const Valuable = require('../Valuable')

const GLUE = ':'
const MAX_PORT = Math.pow(2, 16) - 1

class Port extends Valuable {
    static get GLUE() {
        return GLUE
    }

    /**
     * @param {string|number} [port]
     * @constructor
     */
    constructor(port) {
        if (!isFinite(port) || port < 1 || port > MAX_PORT) {
            port = null
        }

        super(port || '')
    }
}

module.exports = Port

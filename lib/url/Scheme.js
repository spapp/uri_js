/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   URI_js
 * @since     2017-09-26
 */

const Valuable = require('../Valuable')
const GLUE = ':'

class Scheme extends Valuable {
    static get GLUE() {
        return GLUE
    }

    /**
     * @param {string} [scheme]
     * @param {string} [suffix]
     * @constructor
     */
    constructor(scheme, suffix) {
        const value = ('string' === typeof scheme ? scheme : '').split(GLUE)

        if (!suffix && value[1]) {
            suffix = value[1]
        }

        super(value[0])

        this.suffix = 'string' === typeof suffix ? suffix : ''
    }

    /**
     * Returns scheme suffix
     *
     * @return {string}
     */
    getSuffix() {
        return this.suffix
    }
}

module.exports = Scheme

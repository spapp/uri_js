/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   URI_js
 * @since     2017-09-18
 */

const Stringable = require('../Stringable')
const UserInformation = require('./UserInformation')
const Port = require('./Port')
const Host = require('./Host')

/**
 *  username:password@example.com:123
 *  └───────┬───────┘ └────┬────┘ └┬┘
 *  user information     host     port
 *
 * @class
 */
class Authority extends Stringable {
    /**
     * @param {string} authority
     * @constructor
     */
    constructor(authority) {
        let authorityPart, port, userInformation

        super()

        authorityPart = 'string' === typeof authority ? authority.split(UserInformation.GLUE) : [ '' ]
        userInformation = 2 === authorityPart.length ? authorityPart.shift() : null
        authorityPart = authorityPart[0].split(Port.GLUE)
        port = 2 === authorityPart.length ? authorityPart.pop() : null

        this.setUserInformation(new UserInformation(userInformation))
        this.setPort(new Port(port))
        this.setHost(new Host(authorityPart.pop()))
    }

    /**
     * Returns the user information part
     *
     * @return {UserInformation}
     */
    getUserInformation() {
        return this.userInformation
    }

    /**
     * Sets the user information part
     *
     * @param {UserInformation} userInformation
     * @return {Authority}
     */
    setUserInformation(userInformation) {
        this.userInformation = userInformation
        return this
    }

    /**
     * Returns the host part
     *
     * @return {Host}
     */
    getHost() {
        return this.host
    }

    /**
     * Sets the host part
     *
     * @param {Host} host
     * @return {Authority}
     */
    setHost(host) {
        this.host = host
        return this
    }

    /**
     * Returns the port part
     *
     * @return {Port}
     */
    getPort() {
        return this.port
    }

    /**
     * Sets the port part
     *
     * @param {Port} port
     * @return {Authority}
     */
    setPort(port) {
        this.port = port
        return this
    }

    /**
     * Returns URL authority part as a string
     *
     * @return {string}
     */
    toString() {
        const userInformation = this.getUserInformation().toString()
        const port = this.getPort().toString()
        const authority = [ this.getHost().toString() ]

        if (userInformation) {
            authority.unshift(userInformation, UserInformation.GLUE)
        }

        if (port) {
            authority.push(Port.GLUE, port)
        }

        return authority.join('')
    }
}

module.exports = Authority

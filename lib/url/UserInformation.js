/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   URI_js
 * @since     2017-09-20
 */

const GLUE = '@'
const SEPARATOR = ':'

class UserInformation {
    static get GLUE() {
        return GLUE
    }

    /**
     * @param {string} username
     * @param {string} [password]
     * @constructor
     */
    constructor(username, password) {
        let uname = 'string' === typeof username ? username.trim() : ''
        let pwd = 'string' === typeof password ? password.trim() : ''

        if ('' === pwd || uname.indexOf(SEPARATOR) > -1) {
            uname = uname.split(SEPARATOR)
            pwd = uname.length > 1 ? uname.pop() : ''
            uname = uname.shift()
        }

        this.setUsername(uname)
        this.setPassword(pwd)
    }


    /**
     * Returns the username information
     *
     * @return {string}
     */
    getUsername() {
        return this.username
    }

    /**
     * Sets the username information
     *
     * @param {string} username
     * @return {UserInformation}
     */
    setUsername(username) {
        this.username = username
        return this
    }

    /**
     * Returns the password information
     *
     * @return {string}
     */
    getPassword() {
        return this.password
    }

    /**
     * Sets the password information
     *
     * @param {string} password
     * @return {UserInformation}
     */
    setPassword(password) {
        this.password = password
        return this
    }

    /**
     * Returns the user information part of the URL
     *
     * @return {string}
     */
    toString() {
        const username = this.getUsername()
        const pasword = this.getPassword()
        const userInformation = []

        if (username) {
            userInformation.push(username)

            if (pasword) {
                userInformation.push(pasword)
            }
        }

        return userInformation.join(SEPARATOR)
    }
}

module.exports = UserInformation

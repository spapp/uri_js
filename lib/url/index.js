/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   URI_js
 * @since     2017-09-16
 */

/**
 * @see {@link https://en.wikipedia.org/wiki/Uniform_Resource_Identifier}
 * @see {@link https://www.ietf.org/rfc/rfc3986.txt}
 *                     hierarchical part
 *        ┌───────────────────┴─────────────────────┐
 *                    authority               path
 *        ┌───────────────┴───────────────┐┌───┴────┐
 *  abc://username:password@example.com:123/path/data?key=value&key2=value2#fragid1
 *  └┬┘   └───────┬───────┘ └────┬────┘ └┬┘           └─────────┬─────────┘ └──┬──┘
 * scheme  user information    host     port                  query         fragment
 *
 * @param {string} [url]
 * @constructor
 */

const Uri = require('../uri')
const Fragment = require('./Fragment')
const Query = require('./Query')
const Authority = require('./Authority')
const File = require('./File')
const Host = require('./Host')
const Port = require('./Port')
const Scheme = require('./Scheme')
const UserInformation = require('./UserInformation')
const PATTERN_SCHEME = /^[a-z][a-z0-9]*:(\/\/)?/i
const PATTERN_AUTHORITY = /([a-z][a-z0-9]*(:[a-z0-9]+)?@)?([a-z0-9]+\.)+[a-z]{2,4}(:[0-9]+)?/gi
const PATTERN_FILE = /^\.\.?\/|^((\/?[a-z0-9])+\/)+/i

class Url extends Uri {

    static get Fragment() {
        return Fragment
    }

    static get Query() {
        return Query
    }

    static get Authority() {
        return Authority
    }

    static get File() {
        return File
    }

    static get Host() {
        return Host
    }

    static get Port() {
        return Port
    }

    static get Scheme() {
        return Scheme
    }

    static get UserInformation() {
        return UserInformation
    }

    constructor(url) {
        let urlParts = 'string' === typeof url ? url.trim() : ''
        let fragment, scheme, file, query, authority

        super()

        // fragment
        urlParts = urlParts.split(Fragment.GLUE)
        fragment = urlParts[1]
        urlParts = urlParts[0]

        // query
        urlParts = urlParts.split(Query.GLUE)
        query = urlParts[1]
        urlParts = urlParts[0]

        // scheme
        scheme = urlParts.match(PATTERN_SCHEME)

        if (scheme) {
            scheme = scheme[0]
            urlParts = urlParts.replace(PATTERN_SCHEME, '')
        }

        // authority & file
        if (PATTERN_FILE.test(urlParts)) {
            file = urlParts
        } else {
            authority = urlParts.match(PATTERN_AUTHORITY)
            authority = authority ? authority[0] : ''
            file = urlParts.substr(authority.toString().length)
        }

        this.authority = new Authority(authority)
        this.setFragment(new Fragment(fragment))
        this.setQuery(new Query(query))
        this.setScheme(new Scheme(scheme))
        this.setFile(new File(file))
    }

    /**
     * Returns URL fragment
     *
     * @return {Fragment}
     */
    getFragment() {
        return this.fragment
    }

    /**
     * Sets URL fragment
     *
     * @param {Fragment} fragment
     * @return {Url}
     */
    setFragment(fragment) {
        this.fragment = fragment

        return this
    }

    /**
     * Returns URL query
     *
     * @return {Query}
     */
    getQuery() {
        return this.query
    }

    /**
     * Sets URL query
     *
     * @param {Query} query
     * @return {Url}
     */
    setQuery(query) {
        this.query = query

        return this
    }

    /**
     * Sets URL scheme
     *
     * @param {Scheme} scheme
     * @return {Url}
     */
    setScheme(scheme) {
        this.scheme = scheme

        return this
    }

    /**
     * Returns URL scheme
     *
     * @return {Scheme}
     */
    getScheme() {
        return this.scheme
    }

    /**
     * Sets URL file
     *
     * @param {File} file
     * @return {Url}
     */
    setFile(file) {
        this.file = file

        return this
    }

    /**
     * Returns URL file
     *
     * @return {File}
     */
    getFile() {
        return this.file
    }

    /**
     * Returns the user information
     *
     * @return {UserInformation}
     */
    getUserInformation() {
        return this.authority.getUserInformation()
    }

    /**
     * Sets the user information part
     *
     * @param {UserInformation} userInformation
     * @return {Url}
     */
    setUserInformation(userInformation) {
        this.authority.setUserInformation(userInformation)
        return this
    }

    /**
     * Returns the host
     *
     * @return {Host}
     */
    getHost() {
        return this.authority.getHost()
    }

    /**
     * Sets the host
     *
     * @param {Host} host
     * @return {Url}
     */
    setHost(host) {
        this.authority.setHost(host)
        return this
    }

    /**
     * Returns the port
     *
     * @return {Port}
     */
    getPort() {
        return this.authority.getPort()
    }

    /**
     * Sets the port
     *
     * @param {Port} port
     * @return {Url}
     */
    setPort(port) {
        this.authority.setPort(port)
        return this
    }

    /**
     * Returns URL as a string
     *
     * @return {string}
     */
    toString() {
        const url = []
        const scheme = this.getScheme().toString()
        const authority = this.authority.toString()
        const file = this.getFile().toString()
        const query = this.getQuery().toString()
        const fragment = this.getFragment().toString()

        if (scheme) {
            url.push(scheme, Scheme.GLUE, this.getScheme().getSuffix())
        }

        if (authority) {
            url.push(authority)
        }

        if (file) {
            url.push(file)
        }

        if (query) {
            url.push(Query.GLUE, query)
        }

        if (fragment) {
            url.push(Fragment.GLUE, fragment)
        }

        return url.join('')
    }
}

module.exports = Url

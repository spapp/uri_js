/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   URI_js
 * @since     2017-09-20
 */

const Stringable = require('../Stringable')
const SEPARATOR = '.'

class Host extends Stringable {
    /**
     * @param {string} [host]
     * @constructor
     */
    constructor(host) {
        const hostParts = ('string' === typeof host ? host.trim() : '').split(SEPARATOR)

        super()

        this.setTld(hostParts.pop() || '')
        this.setDomain(hostParts.pop() || '')
        this.appendSubDomain.apply(this, hostParts)
    }

    /**
     * Returns top level domain
     *
     * @return {string}
     */
    getTld() {
        return this.tld
    }

    /**
     * Sets up top level domain
     *
     * @param {string} tld
     * @return {Host}
     */
    setTld(tld) {
        this.tld = tld
        return this
    }

    /**
     * Returns domain name
     *
     * @return {string}
     */
    getDomain() {
        return this.domain
    }

    /**
     * Sets up the domain name
     *
     * @param {string} domain
     * @return {Host}
     */
    setDomain(domain) {
        this.domain = domain
        return this
    }

    /**
     * Returns sub domains
     *
     * @return {string}
     */
    getSubDomain() {
        return this.subDomain
    }

    /**
     * Sets up sub domains
     *
     * @param {string} subDomain
     * @return {Host}
     */
    setSubDomain(subDomain) {
        this.subDomain = subDomain
        return this
    }

    /**
     * Appends sub domain(s)
     *
     * @param {...string} subDomain
     * @return {Host}
     */
    appendSubDomain(...subDomain) {
        const oldSubDomains = this.getSubDomain()
        const value = [].concat((oldSubDomains ? oldSubDomains.split(SEPARATOR) : []), subDomain)

        return this.setSubDomain(value.join(SEPARATOR))
    }

    /**
     * Returns the host as a string
     *
     * @return {string}
     */
    toString() {
        const subDomain = this.getSubDomain()
        const domain = this.getDomain()
        const tld = this.getTld()
        const host = []

        if (subDomain) {
            host.push(subDomain)
        }

        if (domain) {
            host.push(domain)
        }

        if (tld) {
            host.push(tld)
        }

        return host.join(SEPARATOR)
    }
}

module.exports = Host

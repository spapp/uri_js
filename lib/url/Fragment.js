/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   URI_js
 * @since     2017-09-16
 */

const Valuable = require('../Valuable')

const GLUE = '#'

class Fragment extends Valuable {
    static get GLUE() {
        return GLUE
    }

    /**
     * @param {string} [fragment]
     * @constructor
     */
    constructor(fragment) {
        super(fragment || '')
    }
}

module.exports = Fragment

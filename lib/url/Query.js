/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   URI_js
 * @since     2017-09-16
 */

const Valuable = require('../Valuable')
const GLUE = '?'
const SEPARATOR_PROPERTY = '&'
const SEPARATOR_KEYVALUE = '='

class Query extends Valuable {
    static get GLUE() {
        return GLUE
    }

    static get SEPARATOR_PROPERTY() {
        return SEPARATOR_PROPERTY
    }

    static get SEPARATOR_KEYVALUE() {
        return SEPARATOR_KEYVALUE
    }

    /**
     * @param {string} [query]
     * @constructor
     */
    constructor(query) {
        let value

        if ('string' === typeof query) {
            value = Query.fromString(query)
        } else if (!isObject(query)) {
            value = {}
        }

        super(value)
    }

    /**
     * Returns URL query sting as an object
     *
     * @param {string} query
     * @param {string} [propertySeparator]
     * @param {string} [keyValueSeparator]
     * @return {{}}
     */
    static fromString(query, propertySeparator, keyValueSeparator) {
        const values = {}
        const properties = query.split(propertySeparator || SEPARATOR_PROPERTY)

        keyValueSeparator = keyValueSeparator || SEPARATOR_KEYVALUE

        for (let i = 0; i < properties.length; i++) {
            const property = properties[i].split(keyValueSeparator)
            const names = property[0].match(/[a-z0-9%_-]+/ig)

            addValue(names, values, property[1])
        }

        return values
    }

    /**
     * Returns a query string
     *
     * @param {object} values
     * @param {string} [propertySeparator]
     * @param {string} [keyValueSeparator]
     * @return {string}
     */
    static toQueryString(values, propertySeparator, keyValueSeparator) {
        const query = []

        if (!isObject(values)) {
            throw new TypeError('Only the object type is supported')
        }

        propertySeparator = propertySeparator || SEPARATOR_PROPERTY
        keyValueSeparator = keyValueSeparator || SEPARATOR_KEYVALUE

        toQueryString(query, values, keyValueSeparator, [])

        return query.join(propertySeparator)
    }

    /**
     * Returns query as a string
     *
     * @return {string}
     */
    toString() {
        return Query.toQueryString(this.value)
    }

    /**
     * Returns TRUE if the property exists
     *
     * @param {string} name property name
     * @return {boolean}
     */
    has(name) {
        const names = Object.keys(this.value)

        return names.indexOf(name) > -1
    }

    /**
     * Sets the new value of the named property
     *
     * @param {string} name property name
     * @param {*} [value] property value
     * @return {Query}
     */
    set(name, value) {
        this.value[name] = value

        return this
    }

    /**
     * Returns the property value if it is exist.
     * If it is not exist then returns the `defaultValue`.
     *
     * @param {string} name property name
     * @param {string} [defaultValue] property default value
     * @return {*}
     */
    get(name, defaultValue) {
        if (this.has(name)) {
            defaultValue = this.value[name]
        }

        return defaultValue
    }

    /**
     * Removes named properties and returns the removed item count.
     *
     * @param {...string} name property name
     * @return {number}
     */
    remove(...name) {
        let removed = 0

        for (const n of name) {
            if (this.has(n)) {
                delete this.value[n]
                ++removed
            }
        }

        return removed
    }
}

function parseValue(value) {
    if ('undefined' === typeof value) {
        // do nothing
    } else if (isNumeric(value)) {
        value = parseFloat(value)
    } else if ('true' === value.toLowerCase()) {
        value = true
    } else if ('false' === value.toLowerCase()) {
        value = false
    } else if ('null' === value.toLowerCase()) {
        value = null
    } else {
        value = decodeURIComponent(value)
    }

    return value
}

function addValue(names, values, value) {
    const name = decodeURIComponent(names.shift())
    const nextName = names[0]

    if (nextName) {
        if (!values[name]) {
            if (isNumeric(nextName)) {
                values[name] = []
            } else {
                values[name] = {}
            }
        }

        addValue(names, values[name], value)
    } else {
        values[name] = parseValue(value)
    }
}

function prepareNames(names, name) {
    if (names.length > 0) {
        name = [ '[', encodeURIComponent(name), ']' ]
    } else {
        name = [ encodeURIComponent(name) ]
    }

    return [].concat(names, name)
}

function toQueryString(query, values, keyValueSeparator, names) {
    if (isObject(values)) {
        const keys = Object.keys(values)

        for (const key of keys) {
            toQueryString(query, values[key], keyValueSeparator, prepareNames(names, key))
        }
    } else if (Array.isArray(values)) {
        const count = values.length

        for (let i = 0; i < count; i++) {
            toQueryString(query, values[i], keyValueSeparator, prepareNames(names, i))
        }
    } else {
        if ('undefined' === typeof values) {
            query.push(names.join(''))
        } else {
            query.push([ names.join(''), encodeURIComponent(values) ].join(keyValueSeparator))
        }
    }
}

function isNumeric(value) {
    return !isNaN(parseFloat(value)) && isFinite(value)
}

function isObject(value) {
    return '[object Object]' === Object.prototype.toString.call(value)
}

module.exports = Query

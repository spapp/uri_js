/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   URI_js
 * @since     2017-09-26
 */

const Valuable = require('../Valuable')

const DIRECTORY_SEPARATOR = '/'

/**
 * @typedef {object} FileInfo
 * @property {string} dirName
 * @property {string} baseName
 * @property {string} fileName
 * @property {string} extension
 */

class File extends Valuable {
    static get DIRECTORY_SEPARATOR() {
        return DIRECTORY_SEPARATOR
    }

    /**
     * @param {string} [file]
     * @param {string} [directorySeparator]
     * @constructor
     */
    constructor(file, directorySeparator) {
        super('string' === typeof file ? file : '')

        this.fileInfo = File.getFileInfo(this.value, directorySeparator)
    }

    /**
     * Returns file informations
     *
     * @static
     * @param {string} file
     * @param {string} [directorySeparator]
     * @return {FileInfo}
     */
    static getFileInfo(file, directorySeparator) {
        const theFile = 'string' === typeof file ? file : ''
        const dirName = theFile.split(directorySeparator || DIRECTORY_SEPARATOR)
        let baseName = dirName.pop()
        let fileName, extension, baseNameParts

        if (baseName) {
            baseNameParts = baseName.split('.')

            if (2 === baseNameParts.length) {
                fileName = baseNameParts[0]
                extension = baseNameParts[1]
            }
        }

        if (!extension) {
            dirName.push(baseName)
            baseName = ''
        }

        return {
            dirName: dirName.join(directorySeparator || DIRECTORY_SEPARATOR),
            baseName: baseName || '',
            fileName: fileName || '',
            extension: extension || ''
        }
    }

    /**
     * Returns the path
     *
     * @return {string}
     */
    getDirectory() {
        return this.fileInfo.dirName
    }

    /**
     * Returns the file base name
     *
     * @return {string}
     */
    getBaseName() {
        return this.fileInfo.baseName
    }

    /**
     * Returns file extension
     *
     * @return {string}
     */
    getExtension() {
        return this.fileInfo.extension
    }

    /**
     * Returns file name
     *
     * @return {string}
     */
    getFileName() {
        return this.fileInfo.fileName
    }
}

module.exports = File

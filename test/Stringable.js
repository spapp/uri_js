/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   URI_js
 * @since     2017-10-01
 */
const Assert = require('assert')
const Test = require('./test')
const { Stringable } = require('../lib')

describe('Stringable', function () {
    Test.toBeFunction(Stringable)

    describe('.toString()', function () {
        Test.toBeFunction(Stringable.prototype.toString)
    })
})

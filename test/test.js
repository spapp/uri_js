const Assert = require('assert')

module.exports = {
    toBeFunction: function (method) {
        it('should be defined', function () {
            Assert.notStrictEqual(undefined, method)
        })

        it('should be a function', function () {
            Assert.strictEqual(typeof method, 'function')
        })
    },
    toThrowError: function (instance, member, params, error) {
        it('should throws a ' + error.name + ' when the parameter is ' + JSON.stringify(params), function () {
            Assert.throws(function () {
                instance[member].apply(instance, params)
            }, error)
        })
    }
}

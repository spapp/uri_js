/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   URI_js
 * @since     2017-10-01
 */
const Assert = require('assert')
const Test = require('./test')
const { Valuable, Stringable } = require('../lib')

describe('Valuable', function () {
    Test.toBeFunction(Valuable)

    it('should implement Stringable', function () {
        Assert.strictEqual(new Valuable() instanceof Stringable, true)
    })
})

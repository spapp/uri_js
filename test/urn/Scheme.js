/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   URI_js
 * @since     2017-09-30
 */

const Assert = require('assert')
const Test = require('../test')
const { Urn, Valuable } = require('../../lib')

describe('Urn.Scheme', function () {
    Test.toBeFunction(Urn.Scheme)

    it('should implement Valuable', function () {
        Assert.strictEqual(new Urn.Scheme() instanceof Valuable, true)
    })

    describe('.toString()', function () {
        Test.toBeFunction(Urn.Scheme.prototype.toString)
    })
})

/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   URI_js
 * @since     2017-09-30
 */

const Assert = require('assert')
const Test = require('../test')
const { Urn } = require('../../lib')

describe('Urn.Path', function () {
    const tests = [
        [ null, '' ],
        [ undefined, '' ],
        [ '', '' ],
        [ [], '' ],
        [ {}, '' ],
        [ 0, '' ],
        [ 12, '' ],
        'isbn:0451450523',
        'isan:0000-0000-9E59-0000-O-0000-0000-2',
        'ISSN:0167-6423',
        'ietf:rfc:2648',
        'mpeg:mpeg7:schema:2001',
        'oid:2.16.840',
        'uuid:6e8bc430-9c3a-11d9-9669-0800200c9a66',
        'lex:eu:council:directive:2010-03-09;2010-19-UE',
        'lsid:zoobank.org:pub:CDC8D258-8F57-41DC-B560-247E17D3DC8C'
    ]

    Test.toBeFunction(Urn.Path)

    describe('.toString()', function () {
        function testPath(actual, expected) {
            it('should return ' + JSON.stringify(expected) + ' when actual is ' + JSON.stringify(actual), function () {
                Assert.strictEqual((new Urn.Path(actual)).toString(), expected)
            })
        }

        Test.toBeFunction(Urn.Path.prototype.toString)

        for (const test of tests) {
            const expected = Array.isArray(test) ? test[1] : test
            const actual = Array.isArray(test) ? test[0] : test

            testPath(actual, expected)
        }
    })
})

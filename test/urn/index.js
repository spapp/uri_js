/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   URI_js
 * @since     2017-09-29
 */

const Assert = require('assert')
const Test = require('../test')
const { Uri, Urn } = require('../../lib')

describe('Urn', function () {
    const urns = [
        null,
        undefined,
        '',
        [],
        {},
        0,
        12,
        'urn:isbn:0451450523',
        'urn:isan:0000-0000-9E59-0000-O-0000-0000-2',
        'urn:ISSN:0167-6423',
        'urn:ietf:rfc:2648',
        'urn:mpeg:mpeg7:schema:2001',
        'urn:oid:2.16.840',
        'urn:uuid:6e8bc430-9c3a-11d9-9669-0800200c9a66',
        'urn:lex:eu:council:directive:2010-03-09;2010-19-UE',
        'urn:lsid:zoobank.org:pub:CDC8D258-8F57-41DC-B560-247E17D3DC8C'
    ]

    function testGetter(method, cls) {
        it('should return a Urn.' + cls.name, function () {
            Assert.strictEqual((new Urn())[method]() instanceof cls, true)
        })
    }

    Test.toBeFunction(Urn)

    it('should be an URI instance', function () {
        Assert.strictEqual(new Urn() instanceof Uri, true)
    })

    describe('.getPath()', function () {
        Test.toBeFunction(Urn.prototype.getPath)
        testGetter('getPath', Urn.Path)
    })

    describe('.setPath()', function () {
        Test.toBeFunction(Urn.prototype.setPath)
    })

    describe('.getScheme()', function () {
        Test.toBeFunction(Urn.prototype.getScheme)
        testGetter('getScheme', Urn.Scheme)
    })

    describe('.setScheme()', function () {
        Test.toBeFunction(Urn.prototype.setScheme)
    })

    describe('.toString()', function () {
        function testUrn(param, expected) {
            it('should return "' + expected + '"', function () {
                Assert.strictEqual((new Urn(param)).toString(), expected)
            })
        }

        Test.toBeFunction(Urn.prototype.toString)

        testUrn(urns[0], '')
        testUrn(urns[1], '')
        testUrn(urns[2], '')
        testUrn(urns[3], '')
        testUrn(urns[4], '')
        testUrn(urns[5], '')
        testUrn(urns[6], '')

        for (let i = 7; i < urns.length; i++) {
            testUrn(urns[i], urns[i])
        }
    })
})

/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   URI_js
 * @since     2017-09-20
 */

const Assert = require('assert')
const Test = require('../test')
const { Url } = require('../../lib')

describe('Url.UserInformation', function () {
    const userInformations = [
        null,
        true,
        [],
        'username:password',
        'username:',
        ':password',
        '',
        ' ',
        undefined
    ]

    function testUserInformation(method, param1, expected, param2) {
        it('should return "' + expected + '"', function () {
            Assert.strictEqual((new Url.UserInformation(param1, param2))[method](), expected)
        })
    }

    Test.toBeFunction(Url.UserInformation)

    describe('setUsername()', function () {
        Test.toBeFunction(Url.UserInformation.prototype.setUsername)
    })

    describe('getUsername()', function () {
        Test.toBeFunction(Url.UserInformation.prototype.getUsername)

        testUserInformation('getUsername', userInformations[0], '')
        testUserInformation('getUsername', userInformations[1], '')
        testUserInformation('getUsername', userInformations[2], '')
        testUserInformation('getUsername', userInformations[3], 'username')
        testUserInformation('getUsername', userInformations[4], 'username')
        testUserInformation('getUsername', userInformations[5], '')
        testUserInformation('getUsername', userInformations[6], '')
        testUserInformation('getUsername', userInformations[7], '')
        testUserInformation('getUsername', userInformations[8], '')
    })

    describe('setPassword()', function () {
        Test.toBeFunction(Url.UserInformation.prototype.setPassword)
    })

    describe('getPassword()', function () {
        Test.toBeFunction(Url.UserInformation.prototype.getPassword)

        testUserInformation('getPassword', userInformations[0], '')
        testUserInformation('getPassword', userInformations[1], '')
        testUserInformation('getPassword', userInformations[2], '')
        testUserInformation('getPassword', userInformations[3], 'password')
        testUserInformation('getPassword', userInformations[4], '')
        testUserInformation('getPassword', userInformations[5], 'password')
        testUserInformation('getPassword', userInformations[6], '')
        testUserInformation('getPassword', userInformations[7], '')
        testUserInformation('getPassword', userInformations[8], '')
    })

    describe('toString()', function () {
        Test.toBeFunction(Url.UserInformation.prototype.toString)

        testUserInformation('toString', userInformations[0], '')
        testUserInformation('toString', userInformations[1], '')
        testUserInformation('toString', userInformations[2], '')
        testUserInformation('toString', userInformations[3], 'username:password')
        testUserInformation('toString', userInformations[4], 'username')
        testUserInformation('toString', userInformations[5], '') // todo perhaps exception
        testUserInformation('toString', userInformations[6], '')
        testUserInformation('toString', userInformations[7], '')
        testUserInformation('toString', userInformations[8], '')

        testUserInformation('toString', '', '', '')
        testUserInformation('toString', null, '', null)
        testUserInformation('toString', 'username', 'username', null)
        testUserInformation('toString', 'username:password', 'username:password', null)
        testUserInformation('toString', 'username:password', 'username:password', 'xxx')
        testUserInformation('toString', '', '', '')
        testUserInformation('toString', 'username:xxxxx:password', 'username:password') // todo
    })
})

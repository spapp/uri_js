/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   URI_js
 * @since     2017-09-18
 */
const Assert = require('assert')
const Test = require('../test')
const { Url } = require('../../lib')

describe('Url.Query', function () {
    const query = 'key=value&key2=value2'
    let test = {
        query: [
            'key3[3][2][k34][0]=0',
            'key3[1]=a',
            'key2[b]=2',
            'key2[c][0]=1',
            'key3[2]=2',
            'key1=key1',
            'key2[a]=1',
            'key2[c][1]=2',
            'key2[c][2][ca]=1',
            'key2[c][2][cb][0]=1',
            'key2[c][2][cb][1]=2',
            'key3[0]=0',
            'key3[3][0]=31',
            'key%20with%20space=value%20with%20space',
            'currency=%24300',
            'key3[3][1]=32',
            'key3[3][2][k33]=1234'
        ].join('&'),
        values: {
            'key with space': 'value with space',
            'currency': '$300',
            key1: 'key1',
            key2: {
                a: 1,
                b: 2,
                c: [ 1, 2, { ca: 1, cb: [ 1, 2 ] } ]
            },
            key3: [ 0, 'a', 2, [ 31, 32, { k33: 1234, k34: [ 0 ] } ] ]
        }
    }

    Test.toBeFunction(Url.Query)

    describe('fromString()', function () {
        function testValue(testValue, type, expected) {
            Assert.notStrictEqual(testValue, undefined)
            Assert.strictEqual(typeof testValue, type)

            if ('undefined' !== typeof expected) {
                Assert.strictEqual(testValue, expected)
            }
        }

        Test.toBeFunction(Url.Query.fromString)

        it('should returns ' + JSON.stringify(test.values), function () {
            const values = Url.Query.fromString(test.query)

            testValue(values, 'object')
            testValue(values['key with space'], 'string', test.values['key with space'])
            testValue(values.currency, 'string', test.values.currency)
            testValue(values.key1, 'string', test.values.key1)

            testValue(values.key2, 'object')
            testValue(values.key2.a, 'number', test.values.key2.a)
            testValue(values.key2.b, 'number', test.values.key2.b)
            testValue(values.key2.c, 'object')
            testValue(values.key2.c[0], 'number', test.values.key2.c[0])
            testValue(values.key2.c[1], 'number', test.values.key2.c[1])
            testValue(values.key2.c[2], 'object')
            testValue(values.key2.c[2].ca, 'number', test.values.key2.c[2].ca)
            testValue(values.key2.c[2].cb, 'object')
            testValue(values.key2.c[2].cb[0], 'number', test.values.key2.c[2].cb[0])
            testValue(values.key2.c[2].cb[1], 'number', test.values.key2.c[2].cb[1])

            testValue(values.key3, 'object')
            testValue(values.key3[0], 'number', test.values.key3[0])
            testValue(values.key3[1], 'string', test.values.key3[1])
            testValue(values.key3[2], 'number', test.values.key3[2])
            testValue(values.key3[3], 'object')
            testValue(values.key3[3][0], 'number', test.values.key3[3][0])
            testValue(values.key3[3][1], 'number', test.values.key3[3][1])
            testValue(values.key3[3][2], 'object')
            testValue(values.key3[3][2].k33, 'number', test.values.key3[3][2].k33)
            testValue(values.key3[3][2].k34, 'object')
            testValue(values.key3[3][2].k34[0], 'number', test.values.key3[3][2].k34[0])
        })

        it('should return "{key:undefined}"', function () {
            Assert.strictEqual(Url.Query.fromString('key').key, undefined)
        })

        it('should return "{key:null}"', function () {
            Assert.strictEqual(Url.Query.fromString('key=null').key, null)
        })

        it('should return "{key:true}"', function () {
            Assert.strictEqual(Url.Query.fromString('key=true').key, true)
        })

        it('should return "{key:false}"', function () {
            Assert.strictEqual(Url.Query.fromString('key=false').key, false)
        })
    })

    describe('toQueryString()', function () {
        const queryParts = test.query.split(Url.Query.SEPARATOR_PROPERTY)
        const count = queryParts.length

        function escapeRegex(string) {
            return string.replace(/([-.*+?\^${}()|\[\]\/\\])/g, '\\$1')
        }

        function testQuery(expected) {
            it('should contains "' + expected + '"', function () {
                const queryString = Url.Query.toQueryString(test.values)
                const regexp = new RegExp(escapeRegex(expected))

                Assert.strictEqual(regexp.test(queryString), true)
            })
        }

        Test.toBeFunction(Url.Query.toQueryString)

        const tests = [ undefined, [], '', 1, null, 'test=12', /[a-z]/, true ]

        for (const test of tests) {
            Test.toThrowError(Url.Query, 'toQueryString', [ test ], TypeError)
        }

        for (let i = 0; i < count; i++) {
            testQuery(queryParts[i])
        }

        it('should return "key"', function () {
            Assert.strictEqual(Url.Query.toQueryString({ key: undefined }), 'key')
        })

        it('should return "key=null"', function () {
            Assert.strictEqual(Url.Query.toQueryString({ key: null }), 'key=null')
        })

        it('should return "key=true"', function () {
            Assert.strictEqual(Url.Query.toQueryString({ key: true }), 'key=true')
        })

        it('should return "key=false"', function () {
            Assert.strictEqual(Url.Query.toQueryString({ key: false }), 'key=false')
        })
    })

    describe('toString()', function () {

        Test.toBeFunction(Url.Query.prototype.toString)

        it('should return "' + query + '"', function () {
            Assert.strictEqual((new Url.Query(query)).toString(), query)
        })
    })

    describe('has()', function () {
        Test.toBeFunction(Url.Query.prototype.has)

        it('should return TRUE', function () {
            Assert.strictEqual((new Url.Query(query)).has('key'), true)
            Assert.strictEqual((new Url.Query(query)).has('key2'), true)
            Assert.strictEqual((new Url.Query(query)).set('a', undefined).has('a'), true)
            Assert.strictEqual((new Url.Query(query)).set('a', null).has('a'), true)
            Assert.strictEqual((new Url.Query(query)).set('a', '').has('a'), true)
            Assert.strictEqual((new Url.Query(query)).set('a', 0).has('a'), true)
        })

        it('should return FALSE', function () {
            Assert.strictEqual((new Url.Query(query)).has('key10'), false)
            Assert.strictEqual((new Url.Query(query)).has('key20'), false)
        })
    })

    describe('get()', function () {

        Test.toBeFunction(Url.Query.prototype.get)

        it('should return "value"', function () {
            Assert.strictEqual((new Url.Query(query)).get('key'), 'value')
        })

        it('should return "value2"', function () {
            Assert.strictEqual((new Url.Query(query)).get('key2'), 'value2')
        })

        it('should return "value2"', function () {
            Assert.strictEqual((new Url.Query(query)).get('key2', 'key2Value'), 'value2')
        })

        it('should return undefined', function () {
            Assert.strictEqual((new Url.Query(query)).get('key3'), undefined)
        })

        it('should return "key3Value"', function () {
            Assert.strictEqual((new Url.Query(query)).get('key3', 'key3Value'), 'key3Value')
        })

        it('should return NULL', function () {
            Assert.strictEqual((new Url.Query(query)).get('key3', null), null)
        })
    })

    describe('set()', function () {
        Test.toBeFunction(Url.Query.prototype.set)

        it('should return 1', function () {
            Assert.strictEqual((new Url.Query(query)).set('key', 1).get('key'), 1)
        })

        it('should return 2', function () {
            Assert.strictEqual((new Url.Query(query)).set('key2', 2).get('key2'), 2)
        })

        it('should return undefined', function () {
            Assert.strictEqual((new Url.Query(query)).set('key').get('key'), undefined)
        })

        it('should return undefined', function () {
            Assert.strictEqual((new Url.Query(query)).set('key').get('key', 'key3Value'), undefined)
        })

        it('should return undefined', function () {
            Assert.strictEqual((new Url.Query(query)).set('key').get('key', null), undefined)
        })
    })

    describe('remove()', function () {
        Test.toBeFunction(Url.Query.prototype.remove)

        it('should return 0', function () {
            Assert.strictEqual((new Url.Query(query)).remove(), 0)
            Assert.strictEqual((new Url.Query(query)).remove('key10'), 0)
            Assert.strictEqual((new Url.Query(query)).remove('key20'), 0)
        })

        it('should return 1', function () {
            Assert.strictEqual((new Url.Query(query)).remove('key10', 'key'), 1)
            Assert.strictEqual((new Url.Query(query)).remove('key20', 'key2'), 1)
        })

        it('should return 2', function () {
            Assert.strictEqual((new Url.Query(query)).remove('key10', 'key', 'key20', 'key2'), 2)
        })

        it('should return "key=value"', function () {
            const urlQuery = new Url.Query(query)

            urlQuery.remove('key2')

            Assert.strictEqual(urlQuery.toString(), 'key=value')
        })

        it('should return ""', function () {
            const urlQuery = new Url.Query(query)

            urlQuery.remove('key10', 'key', 'key20', 'key2')

            Assert.strictEqual(urlQuery.toString(), '')
        })
    })
})

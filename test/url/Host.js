/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   URI_js
 * @since     2017-09-20
 */

const Assert = require('assert')
const Test = require('../test')
const { Url } = require('../../lib')

describe('Url.Host', function () {
    const hosts = [
        null,
        undefined,
        '',
        [],
        {},
        0,
        '0',
        12,
        'example.com',
        'sub1.example.com',
        'sub1.sub2.example.com'
    ]

    function testHost(param, expected, getterMethod, setterMethod, useApply) {
        it('should return "' + expected + '"', function () {
            const host = new Url.Host((setterMethod ? 'sub1.sub2.example.com' : param))

            if (setterMethod) {
                const fn = useApply ? 'apply' : 'call'

                host[setterMethod][fn](host, param)
            }

            Assert.strictEqual(host[getterMethod](), expected)
        })
    }

    Test.toBeFunction(Url.Host)

    describe('getTld()', function () {
        Test.toBeFunction(Url.Host.prototype.getTld)

        testHost(hosts[0], '', 'getTld')
        testHost(hosts[1], '', 'getTld')
        testHost(hosts[2], '', 'getTld')
        testHost(hosts[3], '', 'getTld')
        testHost(hosts[4], '', 'getTld')
        testHost(hosts[5], '', 'getTld')
        // testHost(hosts[6], '', 'getTld'); // todo
        testHost(hosts[7], '', 'getTld')
        testHost(hosts[8], 'com', 'getTld')
        testHost(hosts[9], 'com', 'getTld')
        testHost(hosts[10], 'com', 'getTld')
    })

    describe('setTld()', function () {
        Test.toBeFunction(Url.Host.prototype.setTld)

        testHost('hu', 'hu', 'getTld', 'setTld')
    })

    describe('getDomain()', function () {
        Test.toBeFunction(Url.Host.prototype.getDomain)

        testHost(hosts[0], '', 'getDomain')
        testHost(hosts[1], '', 'getDomain')
        testHost(hosts[2], '', 'getDomain')
        testHost(hosts[3], '', 'getDomain')
        testHost(hosts[4], '', 'getDomain')
        testHost(hosts[5], '', 'getDomain')
        // testHost(hosts[6], '', 'getDomain'); // todo
        testHost(hosts[7], '', 'getDomain')
        testHost(hosts[8], 'example', 'getDomain')
        testHost(hosts[9], 'example', 'getDomain')
        testHost(hosts[10], 'example', 'getDomain')
    })

    describe('setDomain()', function () {
        Test.toBeFunction(Url.Host.prototype.setDomain)

        testHost('test', 'test', 'getDomain', 'setDomain')
    })

    describe('getSubDomain()', function () {
        Test.toBeFunction(Url.Host.prototype.getSubDomain)

        testHost(hosts[0], '', 'getSubDomain')
        testHost(hosts[1], '', 'getSubDomain')
        testHost(hosts[2], '', 'getSubDomain')
        testHost(hosts[3], '', 'getSubDomain')
        testHost(hosts[4], '', 'getSubDomain')
        testHost(hosts[5], '', 'getSubDomain')
        // testHost(hosts[6], '', 'getSubDomain'); // todo
        testHost(hosts[7], '', 'getSubDomain')
        testHost(hosts[8], '', 'getSubDomain')
        testHost(hosts[9], 'sub1', 'getSubDomain')
        testHost(hosts[10], 'sub1.sub2', 'getSubDomain')
    })

    describe('setSubDomain()', function () {
        Test.toBeFunction(Url.Host.prototype.setSubDomain)

        testHost('sub10', 'sub10', 'getSubDomain', 'setSubDomain')
        testHost('sub10.sub11', 'sub10.sub11', 'getSubDomain', 'setSubDomain')
        testHost('sub10.sub11.sub12', 'sub10.sub11.sub12', 'getSubDomain', 'setSubDomain')
    })

    describe('appendSubDomain()', function () {
        Test.toBeFunction(Url.Host.prototype.appendSubDomain)

        testHost('sub10', 'sub1.sub2.sub10', 'getSubDomain', 'appendSubDomain')
        testHost([ 'sub10' ], 'sub1.sub2.sub10', 'getSubDomain', 'appendSubDomain', true)
        testHost([ 'sub10', 'sub11', 'sub12' ], 'sub1.sub2.sub10.sub11.sub12', 'getSubDomain', 'appendSubDomain', true)
    })

    describe('toString()', function () {
        Test.toBeFunction(Url.Host.prototype.toString)

        testHost(hosts[0], '', 'toString')
        testHost(hosts[1], '', 'toString')
        testHost(hosts[2], '', 'toString')
        testHost(hosts[3], '', 'toString')
        testHost(hosts[4], '', 'toString')
        testHost(hosts[5], '', 'toString')
        // testHost(hosts[6], '', 'toString'); // todo
        testHost(hosts[7], '', 'toString')
        testHost(hosts[8], hosts[8], 'toString')
        testHost(hosts[9], hosts[9], 'toString')
        testHost(hosts[10], hosts[10], 'toString')
    })
})

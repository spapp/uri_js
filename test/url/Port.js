/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   URI_js
 * @since     2017-09-20
 */
const Assert = require('assert')
const Test = require('../test')
const { Url } = require('../../lib')

describe('Url.Port', function () {
    const ports = [
        null,
        undefined,
        '',
        [],
        {},
        0,
        '0',
        12,
        '12',
        'abc23',
        80000
    ]

    Test.toBeFunction(Url.Port)

    describe('toString()', function () {
        function testPort(param, expected) {
            it('should return "' + expected + '"', function () {
                Assert.strictEqual((new Url.Port(param)).toString(), expected)
            })
        }

        Test.toBeFunction(Url.Port.prototype.toString)

        testPort(ports[0], '')
        testPort(ports[1], '')
        testPort(ports[2], '')
        testPort(ports[3], '')
        testPort(ports[4], '')
        testPort(ports[5], '')
        testPort(ports[6], '')
        testPort(ports[7], '12')
        testPort(ports[8], '12')
        testPort(ports[9], '')
        testPort(ports[10], '')
    })
})

/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   URI_js
 * @since     2017-09-24
 */
const Assert = require('assert')
const Test = require('../test')
const { Url } = require('../../lib')

describe('Url.Authority', function () {
    const authorities = [
        null,
        undefined,
        '',
        [],
        {},
        0,
        '0',
        12,
        'example.com',
        'sub1.example.com',
        'username:password@example.com:123'
    ]

    Test.toBeFunction(Url.Authority)

    function testAuthority(param, method, expectedCls) {
        it('should return a ' + expectedCls.name + ' instance', function () {
            const authority = new Url.Authority(param)

            Assert.strictEqual(authority[method]() instanceof expectedCls, true)
        })
    }

    function testAuthorities(authorities, method, cls) {
        for (let i = 0; i < authorities.length; i++) {
            testAuthority(authorities[0], method, cls)
        }
    }

    describe('getUserInformation()', function () {
        Test.toBeFunction(Url.Authority.prototype.getUserInformation)

        testAuthorities(authorities, 'getUserInformation', Url.UserInformation)
    })

    describe('setUserInformation()', function () {
        Test.toBeFunction(Url.Authority.prototype.setUserInformation)
    })

    describe('getHost()', function () {
        Test.toBeFunction(Url.Authority.prototype.getHost)

        testAuthorities(authorities, 'getHost', Url.Host)
    })

    describe('setHost()', function () {
        Test.toBeFunction(Url.Authority.prototype.setHost)
    })

    describe('getPort()', function () {
        Test.toBeFunction(Url.Authority.prototype.getPort)

        testAuthorities(authorities, 'getPort', Url.Port)
    })

    describe('setPort()', function () {
        Test.toBeFunction(Url.Authority.prototype.setPort)
    })

    describe('toString()', function () {
        function testAuthority(param, expected) {
            it('should return "' + expected + '"', function () {
                Assert.strictEqual((new Url.Authority(param)).toString(), expected)
            })
        }

        Test.toBeFunction(Url.Authority.prototype.toString)

        testAuthority(authorities[0], '')
        testAuthority(authorities[1], '')
        testAuthority(authorities[2], '')
        testAuthority(authorities[3], '')
        testAuthority(authorities[4], '')
        testAuthority(authorities[5], '')
        // testAuthority(authorities[6], ''); // todo '0'
        testAuthority(authorities[7], '')
        testAuthority(authorities[8], authorities[8])
        testAuthority(authorities[9], authorities[9])
        testAuthority(authorities[10], authorities[10])
    })
})

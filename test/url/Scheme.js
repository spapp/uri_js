/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   URI_js
 * @since     2017-09-27
 */

const Assert = require('assert')
const Test = require('../test')
const { Url } = require('../../lib')

describe('Url.Scheme', function () {
    const schemes = [
        null,
        undefined,
        '',
        [],
        {},
        0,
        12,
        'http',
        'http://',
        'mail:',
        'tel:'
    ]

    Test.toBeFunction(Url.Scheme)

    describe('getSuffix()', function () {
        function testScheme(param, expected) {
            it('should return "' + expected + '"', function () {
                Assert.strictEqual((new Url.Scheme(param)).getSuffix(), expected)
            })
        }

        Test.toBeFunction(Url.Scheme.prototype.getSuffix)

        testScheme(schemes[0], '')
        testScheme(schemes[1], '')
        testScheme(schemes[2], '')
        testScheme(schemes[3], '')
        testScheme(schemes[4], '')
        testScheme(schemes[5], '')
        testScheme(schemes[6], '')
        testScheme(schemes[7], '')
        testScheme(schemes[8], '//')
        testScheme(schemes[9], '')
        testScheme(schemes[10], '')

        it('should return "//"', function () {
            Assert.strictEqual((new Url.Scheme('http', '//')).getSuffix(), '//')
        })

        it('should return "--"', function () {
            Assert.strictEqual((new Url.Scheme('teszt', '--')).getSuffix(), '--')
        })
    })

    describe('toString()', function () {
        function testScheme(param, expected) {
            it('should return "' + expected + '"', function () {
                Assert.strictEqual((new Url.Scheme(param)).toString(), expected)
            })
        }

        Test.toBeFunction(Url.Scheme.prototype.toString)

        testScheme(schemes[0], '')
        testScheme(schemes[1], '')
        testScheme(schemes[2], '')
        testScheme(schemes[3], '')
        testScheme(schemes[4], '')
        testScheme(schemes[5], '')
        testScheme(schemes[6], '')
        testScheme(schemes[7], 'http')
        testScheme(schemes[8], 'http')
        testScheme(schemes[9], 'mail')
        testScheme(schemes[10], 'tel')
    })
})

/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   URI_js
 * @since     2017-09-16
 */
const Assert = require('assert')
const Test = require('../test')
const { Url } = require('../../lib')

describe('Url.Fragment', function () {
    Test.toBeFunction(Url.Fragment)

    describe('toString()', function () {
        Test.toBeFunction(Url.Fragment.prototype.toString)

        it('should return "fragment"', function () {
            Assert.strictEqual((new Url.Fragment('fragment')).toString(), 'fragment')
        })
    })
})

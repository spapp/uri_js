/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   URI_js
 * @since     2017-09-27
 */
const Assert = require('assert')
const Test = require('../test')
const { Url } = require('../../lib')

describe('Url.File', function () {
    const files = [
        null,
        undefined,
        '',
        [],
        {},
        0,
        12,
        '/relative/URI/with/absolute/path/to/resource.txt',
        'relative/path/to/resource.txt',
        '../../../resource.txt',
        './resource.txt',
        'resource.txt'
    ]

    function testFilePart(param, expected, method) {
        it('should return "' + expected + '"', function () {
            Assert.strictEqual((new Url.File(param))[method](), expected)
        })
    }

    Test.toBeFunction(Url.File)

    describe('getFileInfo()', function () {
        let empty

        function testFile(param, expected) {
            it('should return "' + JSON.stringify(expected) + '"', function () {
                Assert.deepStrictEqual(Url.File.getFileInfo(param), expected)
            })
        }

        Test.toBeFunction(Url.File.getFileInfo)

        empty = {
            dirName: '',
            baseName: '',
            fileName: '',
            extension: ''
        }

        testFile(files[0], empty)
        testFile(files[1], empty)
        testFile(files[2], empty)
        testFile(files[3], empty)
        testFile(files[4], empty)
        testFile(files[5], empty)
        testFile(files[6], empty)
        testFile(files[7], {
            dirName: '/relative/URI/with/absolute/path/to',
            baseName: 'resource.txt',
            fileName: 'resource',
            extension: 'txt'
        })
        testFile(files[8], {
            dirName: 'relative/path/to',
            baseName: 'resource.txt',
            fileName: 'resource',
            extension: 'txt'
        })
        testFile(files[9], {
            dirName: '../../..',
            baseName: 'resource.txt',
            fileName: 'resource',
            extension: 'txt'
        })
        testFile(files[10], {
            dirName: '.',
            baseName: 'resource.txt',
            fileName: 'resource',
            extension: 'txt'
        })
        testFile(files[11], {
            dirName: '',
            baseName: 'resource.txt',
            fileName: 'resource',
            extension: 'txt'
        })
        testFile('relative/path/to', {
            dirName: 'relative/path/to',
            baseName: '',
            fileName: '',
            extension: ''
        })
    })

    describe('toString()', function () {
        function testFile(param, expected) {
            it('should return "' + expected + '"', function () {
                Assert.strictEqual(String(new Url.File(param)), expected)
            })
        }

        Test.toBeFunction(Url.File.prototype.toString)

        testFile(files[0], '')
        testFile(files[1], '')
        testFile(files[2], '')
        testFile(files[3], '')
        testFile(files[4], '')
        testFile(files[5], '')
        testFile(files[6], '')
        testFile(files[7], files[7])
        testFile(files[8], files[8])
        testFile(files[9], files[9])
        testFile(files[10], files[10])
        testFile(files[11], files[11])
    })

    describe('getDirectory()', function () {
        Test.toBeFunction(Url.File.prototype.getDirectory)

        testFilePart(files[7], '/relative/URI/with/absolute/path/to', 'getDirectory')
        testFilePart(files[8], 'relative/path/to', 'getDirectory')
        testFilePart(files[9], '../../..', 'getDirectory')
        testFilePart(files[10], '.', 'getDirectory')
        testFilePart('relative/path/to', 'relative/path/to', 'getDirectory')
        testFilePart(files[11], '', 'getDirectory')
    })

    describe('getBaseName()', function () {

        Test.toBeFunction(Url.File.prototype.getBaseName)

        testFilePart(files[7], 'resource.txt', 'getBaseName')
        testFilePart(files[8], 'resource.txt', 'getBaseName')
        testFilePart(files[9], 'resource.txt', 'getBaseName')
        testFilePart(files[10], 'resource.txt', 'getBaseName')
        testFilePart(files[11], 'resource.txt', 'getBaseName')
    })

    describe('getExtension()', function () {
        Test.toBeFunction(Url.File.prototype.getExtension)

        testFilePart(files[7], 'txt', 'getExtension')
        testFilePart(files[8], 'txt', 'getExtension')
        testFilePart(files[9], 'txt', 'getExtension')
        testFilePart(files[10], 'txt', 'getExtension')
        testFilePart('relative/path/to', '', 'getExtension')
        testFilePart(files[11], 'txt', 'getExtension')
    })

    describe('getFileName()', function () {
        Test.toBeFunction(Url.File.prototype.getFileName)

        testFilePart(files[7], 'resource', 'getFileName')
        testFilePart(files[8], 'resource', 'getFileName')
        testFilePart(files[9], 'resource', 'getFileName')
        testFilePart(files[10], 'resource', 'getFileName')
        testFilePart('relative/path/to', '', 'getFileName')
        testFilePart(files[11], 'resource', 'getFileName')
    })
})

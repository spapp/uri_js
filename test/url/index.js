/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2017
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   URI_js
 * @since     2017-09-16
 */

const Assert = require('assert')
const Test = require('../test')
const { Url, Uri } = require('../../lib')

describe('Url', function () {
    const query = 'key=value&key2=value2'
    const urls = [
        null,
        undefined,
        '',
        [],
        {},
        0,
        12,
        'abc://username:password@example.com:123/path/data?' + query + '#fragid1',
        'abc://username:password@example.com:123/path/data?' + query,
        'abc://username:password@example.com:123/path/data',
        'https://example.org/absolute/URI/with/absolute/path/to/resource.txt',
        'https://example.org/absolute/URI/with/absolute/path/to/resource',
        'ftp://example.org/resource.txt',
        '/relative/URI/with/absolute/path/to/resource.txt',
        'relative/path/to/resource.txt',
        '../../../resource.txt',
        './resource.txt#frag01',
        'resource.txt',
        '#frag01',
        'mailto:someone@example.com',
        'mailto:someone@example.com?subject=This%20is%20the%20subject&cc=someone_else%40example.com&body=This%20is%20the%20body',
        'data:image/png;base64,base64encodedimage',
        'irc://host:123/channel?channel_keyword',
        'ircs://host:456/channel',
        'irc6://host:676767'
    ]

    function testGetter(method, cls) {
        it('should return a ' + cls.name, function () {
            Assert.strictEqual((new Url())[method]() instanceof cls, true)
        })
    }

    Test.toBeFunction(Url)

    it('should be an URI instance', function () {
        Assert.strictEqual((new Url()) instanceof Uri, true)
    })

    describe('getFragment()', function () {
        Test.toBeFunction(Url.prototype.getFragment)
        testGetter('getFragment', Url.Fragment)
    })

    describe('setFragment()', function () {
        Test.toBeFunction(Url.prototype.setFragment)
    })

    describe('getQuery()', function () {
        Test.toBeFunction(Url.prototype.getQuery)
        testGetter('getQuery', Url.Query)
    })

    describe('setQuery()', function () {
        Test.toBeFunction(Url.prototype.setQuery)
    })

    describe('setScheme()', function () {
        Test.toBeFunction(Url.prototype.setScheme)
    })

    describe('getScheme()', function () {
        Test.toBeFunction(Url.prototype.getScheme)
        testGetter('getScheme', Url.Scheme)
    })

    describe('setFile()', function () {
        Test.toBeFunction(Url.prototype.setFile)
    })

    describe('getFile()', function () {
        Test.toBeFunction(Url.prototype.getFile)
        testGetter('getFile', Url.File)
    })

    describe('getUserInformation()', function () {
        Test.toBeFunction(Url.prototype.getUserInformation)
        testGetter('getUserInformation', Url.UserInformation)
    })

    describe('setUserInformation()', function () {
        Test.toBeFunction(Url.prototype.setUserInformation)
    })

    describe('setHost()', function () {
        Test.toBeFunction(Url.prototype.setHost)
    })

    describe('getHost()', function () {
        Test.toBeFunction(Url.prototype.getHost)
        testGetter('getHost', Url.Host)
    })

    describe('getPort()', function () {
        Test.toBeFunction(Url.prototype.getPort)
        testGetter('getPort', Url.Port)
    })

    describe('setPort()', function () {
        Test.toBeFunction(Url.prototype.setPort)
    })

    describe('toString()', function () {
        function testUrl(param, expected) {
            it('should return "' + expected + '"', function () {
                Assert.strictEqual((new Url(param)).toString(), expected)
            })
        }

        Test.toBeFunction(Url.prototype.toString)

        testUrl(urls[0], '')
        testUrl(urls[1], '')
        testUrl(urls[2], '')
        testUrl(urls[3], '')
        testUrl(urls[4], '')
        testUrl(urls[5], '')
        testUrl(urls[6], '')

        for (let i = 7; i < urls.length; i++) {
            testUrl(urls[i], urls[i])
        }
    })
})
